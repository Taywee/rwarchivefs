use std::path::PathBuf;

use clap::Parser;
use fuser::mount2;
use fuser::MountOption;
use rwarchivefs::archive::Archive;
use rwarchivefs::archive::ArchiveConfig;
use rwarchivefs::filesystem::Filesystem;
use simplelog::{ColorChoice, Config, TermLogger, TerminalMode};

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    #[clap(short, long)]
    archive: PathBuf,

    #[clap(short, long)]
    mountpoint: PathBuf,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    TermLogger::init(
        log::LevelFilter::Debug,
        Config::default(),
        TerminalMode::Mixed,
        ColorChoice::Auto,
    )?;

    let args = Args::parse();
    let mut options = vec![MountOption::FSName("fuser".to_string())];

    options.push(MountOption::AutoUnmount);
    options.push(MountOption::AllowOther);
    options.push(MountOption::FSName(
        args.archive.clone().display().to_string(),
    ));
    options.push(MountOption::Subtype("rwarchivefs".into()));
    options.push(MountOption::NoDev);
    options.push(MountOption::NoSuid);
    options.push(MountOption::RW);
    options.push(MountOption::NoExec);
    options.push(MountOption::NoAtime);
    options.push(MountOption::Sync);
    let archive = Archive::open(args.archive, ArchiveConfig::default())?;
    let filesystem = Filesystem::new(archive);
    std::fs::create_dir_all(&args.mountpoint)?;
    Ok(mount2(filesystem, args.mountpoint, &options)?)
}
