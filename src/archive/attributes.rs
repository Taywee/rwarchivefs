use rusqlite::{params, Connection};

use crate::{
    archive::{mode::Mode, Error},
    include_sql,
};
use std::{
    ffi::OsStr,
    os::unix::prelude::OsStrExt,
    time::{Duration, SystemTime, UNIX_EPOCH},
};

/** Attributes needed for grabbing file attributes and such.
 */
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct InodeAttributes {
    pub id: u64,
    pub mode: Mode,
    pub links: u32,
    pub user_id: u32,
    pub group_id: u32,
    pub size: u64,
    pub btime: SystemTime,
    pub ctime: SystemTime,
    pub mtime: SystemTime,
    pub atime: SystemTime,
}

fn system_time(seconds: i64, nanoseconds: u32) -> SystemTime {
    if seconds >= 0 {
        UNIX_EPOCH + Duration::new(seconds as u64, nanoseconds)
    } else {
        UNIX_EPOCH - Duration::from_secs(seconds as u64) + Duration::from_nanos(nanoseconds as u64)
    }
}

impl InodeAttributes {
    pub fn for_inode(connection: &Connection, inode: u64) -> Result<Self, Error> {
        let mut statement = connection.prepare_cached(include_sql!("select_inode_attributes"))?;
        let attributes = statement.query_row([inode], |row| {
            Ok(InodeAttributes {
                id: inode,
                mode: row.get(0)?,
                links: row.get(1)?,
                user_id: row.get(2)?,
                group_id: row.get(3)?,
                size: row.get(4)?,
                btime: system_time(row.get(5)?, row.get(6)?),
                ctime: system_time(row.get(7)?, row.get(8)?),
                mtime: system_time(row.get(9)?, row.get(10)?),
                atime: system_time(row.get(11)?, row.get(12)?),
            })
        })?;
        Ok(attributes)
    }

    pub fn for_directory_entry(
        connection: &Connection,
        directory: u64,
        name: &OsStr,
    ) -> Result<Self, Error> {
        let mut statement =
            connection.prepare_cached(include_sql!("select_directory_entry_attributes"))?;
        let attributes = statement.query_row(params![directory, name.as_bytes()], |row| {
            Ok(InodeAttributes {
                id: row.get(0)?,
                mode: row.get(1)?,
                links: row.get(2)?,
                user_id: row.get(3)?,
                group_id: row.get(4)?,
                size: row.get(5)?,
                btime: system_time(row.get(6)?, row.get(7)?),
                ctime: system_time(row.get(8)?, row.get(9)?),
                mtime: system_time(row.get(10)?, row.get(11)?),
                atime: system_time(row.get(12)?, row.get(13)?),
            })
        })?;
        Ok(attributes)
    }
}

/** Attributes needed for setting file attributes on an existing inode.
 */
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct SetInodeAttributes {
    pub mode: Option<Mode>,
    pub user_id: Option<u32>,
    pub group_id: Option<u32>,
    pub btime: Option<SystemTime>,
    pub ctime: Option<SystemTime>,
    pub mtime: Option<SystemTime>,
    pub atime: Option<SystemTime>,
    pub size: Option<u64>,
}

/** Attributes needed for grabbing file attributes and such.
 */
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct FileAttributes {
    pub inode_attributes: InodeAttributes,
    pub blocks: u64,
    pub block_size: u32,
}

impl FileAttributes {
    pub fn for_inode(connection: &Connection, inode: u64) -> Result<Self, Error> {
        let inode_attributes = InodeAttributes::for_inode(&connection, inode)?;
        let mut statement = connection.prepare_cached("SELECT block_size FROM metadata")?;
        let block_size = statement.query_row([], |row| row.get(0))?;

        let mut statement =
            connection.prepare_cached("SELECT COUNT(*) FROM file_blocks WHERE inode = ?")?;
        let blocks = statement.query_row([inode], |row| row.get(0))?;
        Ok(FileAttributes {
            inode_attributes,
            blocks,
            block_size,
        })
    }
    pub fn for_directory_entry(
        connection: &Connection,
        directory: u64,
        name: &OsStr,
    ) -> Result<Self, Error> {
        let inode_attributes = InodeAttributes::for_directory_entry(&connection, directory, name)?;
        let mut statement = connection.prepare_cached("SELECT block_size FROM metadata")?;
        let block_size = statement.query_row([], |row| row.get(0))?;

        let mut statement =
            connection.prepare_cached("SELECT COUNT(*) FROM file_blocks WHERE inode = ?")?;
        let blocks = statement.query_row([inode_attributes.id], |row| row.get(0))?;
        Ok(FileAttributes {
            inode_attributes,
            blocks,
            block_size,
        })
    }
}

impl From<FileAttributes> for fuser::FileAttr {
    fn from(attributes: FileAttributes) -> Self {
        fuser::FileAttr {
            ino: attributes.inode_attributes.id,
            size: attributes.inode_attributes.size,
            atime: attributes.inode_attributes.atime,
            mtime: attributes.inode_attributes.mtime,
            ctime: attributes.inode_attributes.ctime,
            crtime: attributes.inode_attributes.btime,
            blocks: attributes.blocks,
            perm: attributes.inode_attributes.mode.permissions(),
            kind: attributes.inode_attributes.mode.kind(),
            nlink: attributes.inode_attributes.links,
            uid: attributes.inode_attributes.user_id,
            gid: attributes.inode_attributes.group_id,
            rdev: 0,
            blksize: attributes.block_size,
            flags: 0,
        }
    }
}
