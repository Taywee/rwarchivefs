use std::{fmt, io, os::raw::c_int};

#[derive(Debug)]
pub enum Error {
    Rusqlite(rusqlite::Error),
    WrongApplicationId,
    NoSuchInode(u64),
    InvalidHandle,
    NotFile,
    NotFound,
    NotLink,
    NotDirectory,
    AlreadyExists(u64),
    PositionTooBig,
    PositionNegative,
    FilesystemClosed,
}

impl Error {
    pub fn errno(&self) -> c_int {
        match self {
            Error::Rusqlite(rusqlite::Error::QueryReturnedNoRows) => libc::ENOENT,
            Error::Rusqlite(rusqlite::Error::SqliteFailure(
                rusqlite::ffi::Error {
                    code: rusqlite::ffi::ErrorCode::ReadOnly,
                    ..
                },
                ..,
            )) => libc::EBADF,
            Error::Rusqlite(rusqlite::Error::SqliteFailure(
                rusqlite::ffi::Error {
                    code: rusqlite::ffi::ErrorCode::ConstraintViolation,
                    ..
                },
                ..,
            )) => libc::EEXIST,
            Error::Rusqlite(_) => libc::EIO,
            Error::WrongApplicationId => libc::EINVAL,
            Error::NoSuchInode(_) => libc::ENOENT,
            Error::InvalidHandle => libc::EBADF,
            Error::NotFile => libc::EINVAL,
            Error::NotFound => libc::ENOENT,
            Error::NotLink => libc::EINVAL,
            Error::NotDirectory => libc::ENOTDIR,
            Error::AlreadyExists(_) => libc::EEXIST,
            Error::PositionTooBig => libc::EINVAL,
            Error::PositionNegative => libc::EINVAL,
            Error::FilesystemClosed => libc::EIO,
        }
    }
}

impl From<rusqlite::Error> for Error {
    fn from(v: rusqlite::Error) -> Self {
        Self::Rusqlite(v)
    }
}

impl std::error::Error for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Error::Rusqlite(e) => write!(f, "Rusqlite: {}", e),
            Error::WrongApplicationId => write!(f, "WrongApplicationId"),
            Error::NoSuchInode(inode) => write!(f, "NoSuchInode: {}", inode),
            Error::InvalidHandle => write!(f, "InvalidHandle"),
            Error::NotFile => write!(f, "NotFile"),
            Error::NotFound => write!(f, "NotFound"),
            Error::NotLink => write!(f, "NotLink"),
            Error::NotDirectory => write!(f, "NotDirectory"),
            Error::AlreadyExists(inode) => write!(f, "AlreadyExists: {}", inode),
            Error::PositionTooBig => write!(f, "PositionTooBig"),
            Error::PositionNegative => write!(f, "PositionNegative"),
            Error::FilesystemClosed => write!(f, "FilesystemClosed"),
        }
    }
}

impl From<Error> for io::Error {
    fn from(error: Error) -> Self {
        io::Error::new(io::ErrorKind::Other, error)
    }
}
