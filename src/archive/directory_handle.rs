use std::{
    cell::RefCell,
    ffi::{OsStr, OsString},
    rc::{Rc, Weak},
};

use crate::include_sql;

use super::mode::Mode;
use super::Error;
use rusqlite::{params, Connection};
use std::os::unix::ffi::{OsStrExt, OsStringExt};

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
enum DirectoryHandleState {
    Dot,
    DotDot,
    Children { after: Option<OsString> },
    Done,
}

impl Default for DirectoryHandleState {
    fn default() -> Self {
        DirectoryHandleState::Dot
    }
}

#[derive(Clone, Debug)]
pub struct DirectoryEntry {
    inode: u64,
    name: OsString,
    mode: Mode,
}

impl DirectoryEntry {
    /// Get the directory entry's inode.
    pub fn inode(&self) -> u64 {
        self.inode
    }

    /// Get a reference to the directory entry's name.
    pub fn name(&self) -> &OsStr {
        &self.name
    }

    pub fn mode(&self) -> Mode {
        self.mode
    }
}

#[derive(Default, Debug)]
pub struct DirectoryHandle {
    inode: u64,

    state: DirectoryHandleState,

    connection: Weak<RefCell<Connection>>,
}

impl DirectoryHandle {
    pub fn new(inode: u64, connection: Rc<RefCell<Connection>>) -> Self {
        DirectoryHandle {
            inode,
            connection: Rc::downgrade(&connection),
            ..Default::default()
        }
    }

    /// Get a reference to the directory handle's inode.
    pub fn inode(&self) -> u64 {
        self.inode
    }

    /// Inverted next, to allow proper try operator operation.
    fn inner_next(&mut self) -> Result<Option<DirectoryEntry>, Error> {
        let connection = match self.connection.upgrade() {
            Some(c) => c,
            None => return Err(Error::InvalidHandle),
        };

        let connection = connection.borrow_mut();
        match self.state {
            DirectoryHandleState::Dot => {
                self.state = DirectoryHandleState::DotDot;
                let mut statement =
                    connection.prepare_cached("SELECT mode FROM inodes WHERE id = ?")?;
                let mode = statement.query_row(params![self.inode], |row| row.get(0))?;

                Ok(Some(DirectoryEntry {
                    inode: self.inode,
                    name: ".".into(),
                    mode: Mode::from_raw(mode),
                }))
            }
            DirectoryHandleState::DotDot => {
                self.state = DirectoryHandleState::Children { after: None };
                let mut statement = connection.prepare_cached("SELECT directory, mode FROM directory_entries JOIN inodes on inodes.id = directory WHERE inode = ?")?;
                let result = statement.query_row(params![self.inode], |row| {
                    Ok(DirectoryEntry {
                        name: "..".into(),
                        inode: row.get(0)?,
                        mode: Mode::from_raw(row.get(1)?),
                    })
                });
                match result {
                    Err(rusqlite::Error::QueryReturnedNoRows) => {
                        let mut statement =
                            connection.prepare_cached("SELECT mode FROM inodes WHERE id = ?")?;
                        let mode = statement.query_row(params![self.inode], |row| row.get(0))?;
                        Ok(Some(DirectoryEntry {
                            name: "..".into(),
                            // Maybe?  Not sure what to return for the root directory
                            inode: self.inode,
                            mode: Mode::from_raw(mode),
                        }))
                    }
                    e => Ok(Some(e?)),
                }
            }
            DirectoryHandleState::Children { ref after } => {
                let result = if let Some(after) = after {
                    let mut statement =
                        connection.prepare_cached(include_sql!("select_directory_entry_after"))?;
                    statement.query_row(params![self.inode, after.as_os_str().as_bytes()], |row| {
                        let name = OsString::from_vec(row.get(0)?);
                        Ok(DirectoryEntry {
                            name,
                            inode: row.get(1)?,
                            mode: Mode::from_raw(row.get(2)?),
                        })
                    })
                } else {
                    let mut statement =
                        connection.prepare_cached(include_sql!("select_directory_entry_first"))?;
                    statement.query_row(params![self.inode], |row| {
                        let name = OsString::from_vec(row.get(0)?);
                        Ok(DirectoryEntry {
                            name,
                            inode: row.get(1)?,
                            mode: Mode::from_raw(row.get(2)?),
                        })
                    })
                };
                match result {
                    Err(rusqlite::Error::QueryReturnedNoRows) => {
                        self.state = DirectoryHandleState::Done;
                        Ok(None)
                    }
                    e => {
                        let entry = e?;
                        self.state = DirectoryHandleState::Children {
                            after: Some(entry.name.clone()),
                        };
                        Ok(Some(entry))
                    }
                }
            }
            DirectoryHandleState::Done => Ok(None),
        }
    }
}

impl Iterator for DirectoryHandle {
    type Item = Result<DirectoryEntry, Error>;

    fn next(&mut self) -> Option<Self::Item> {
        if matches!(self.state, DirectoryHandleState::Done) {
            return None;
        }
        self.inner_next().transpose()
    }
}
