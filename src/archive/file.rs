use super::Error;

use log::{error, info};
use rusqlite::{params, Connection, DropBehavior, OptionalExtension, TransactionBehavior};
use std::{
    cell::RefCell,
    io::{self, Write},
    rc::{Rc, Weak},
};

/** Load a block if it exists.
*/
pub fn load_block(
    connection: &Connection,
    inode: u64,
    block_number: u64,
) -> Result<Option<Vec<u8>>, Error> {
    let mut statement = connection
        .prepare_cached("SELECT data FROM file_blocks WHERE inode = ? AND block_number = ?")?;
    Ok(statement
        .query_row(params![inode, block_number], |row| row.get(0))
        .optional()?)
}

/** Store a block as the given block number.
*/
pub fn store_block(
    connection: &Connection,
    inode: u64,
    number: u64,
    data: &[u8],
) -> Result<(), Error> {
    info!("Storing block");
    let mut statement = connection.prepare_cached("INSERT INTO file_blocks (inode, block_number, data) VALUES (?1, ?2, ?3) ON CONFLICT(inode, block_number) DO UPDATE SET data=excluded.data")?;
    statement.execute(params![inode, number, data])?;
    Ok(())
}

/** Set the file's size.
 *
 * A trigger deletes removed blocks, and this fills remaining blocks with zeros.
 */
pub fn set_file_size(connection: &Connection, inode: u64, size: u64) -> Result<(), Error> {
    let mut statement = connection.prepare_cached("SELECT block_size FROM metadata")?;
    let block_size: u32 = statement.query_row([], |row| row.get(0))?;
    let mut statement = connection.prepare_cached("SELECT size FROM inodes WHERE id=?")?;
    let previous_size: u64 = statement.query_row([inode], |row| row.get(0))?;
    let mut statement = connection.prepare_cached("UPDATE inodes SET size=? WHERE id=?")?;
    statement.execute(params![size, inode])?;
    if size < previous_size {
        let remainder = (size % block_size as u64) as u32;
        let last_block = if remainder == 0 {
            (size / block_size as u64).checked_sub(1)
        } else {
            Some(size / block_size as u64)
        };
        if remainder > 0 {
            if let Some(block_number) = last_block {
                if let Some(mut block) = load_block(connection, inode, block_number)? {
                    (&mut block[remainder as usize..]).fill(0);
                    store_block(connection, inode, block_number, &block)?;
                }
            }
        }
    }
    Ok(())
}

pub fn file_size(connection: &Connection, inode: u64) -> Result<u64, Error> {
    let mut statement = connection.prepare_cached("SELECT size FROM inodes WHERE id = ?")?;
    statement
        .query_row(params![inode], |row| row.get(0))
        .map_err(From::from)
}

#[derive(Default, Debug)]
pub struct Handle {
    open_inode_id: i64,

    inode: u64,

    block_size: u32,

    /// The open block number.
    open_block_number: u64,

    /// The cursor offset into the current open block.
    open_block_offset: u32,

    connection: Weak<RefCell<Connection>>,
}

impl Handle {
    pub fn new(inode: u64, connection: Rc<RefCell<Connection>>) -> Result<Self, Error> {
        let (block_size, open_inode_id) = {
            let mut connection = connection.borrow_mut();
            let transaction =
                connection.transaction_with_behavior(TransactionBehavior::Immediate)?;
            let (block_size, open_inode_id) = {
                let mut statement =
                    transaction.prepare_cached("SELECT block_size FROM metadata")?;
                let block_size = statement.query_row([], |row| row.get(0))?;

                let mut statement = transaction
                    .prepare_cached("INSERT INTO temp.open_inodes (inode) VALUES (?)")?;
                statement.execute([inode])?;
                let open_inode_id = transaction.last_insert_rowid();
                (block_size, open_inode_id)
            };
            transaction.commit()?;
            (block_size, open_inode_id)
        };
        Ok(Handle {
            open_inode_id,
            inode,
            block_size,
            open_block_number: 0,
            open_block_offset: 0,
            connection: Rc::downgrade(&connection),
        })
    }

    fn cursor_position(&self) -> u64 {
        self.block_size as u64 * self.open_block_number + self.open_block_offset as u64
    }

    fn set_cursor(&mut self, position: u64) {
        self.open_block_number = position / self.block_size as u64;
        self.open_block_offset = (position % self.block_size as u64) as u32;
    }

    /// Get the file handle's inode.
    pub fn inode(&self) -> u64 {
        self.inode
    }
}

impl io::Read for Handle {
    fn read(&mut self, mut buf: &mut [u8]) -> Result<usize, io::Error> {
        let connection = self.connection.upgrade().ok_or(Error::FilesystemClosed)?;
        let mut connection = connection.borrow_mut();

        // Read transaction just for efficient read behavior
        let mut transaction = connection.transaction().map_err(Error::from)?;
        transaction.set_drop_behavior(DropBehavior::Commit);
        let file_size = file_size(&transaction, self.inode)?;

        let mut count = 0;
        while !buf.is_empty() {
            let left_in_block = self.block_size - self.open_block_offset;
            let left_in_file = file_size - self.cursor_position();
            let amount_to_read = (buf.len() as u64)
                .min(left_in_block as u64)
                .min(left_in_file) as u32;
            if amount_to_read == 0 {
                return Ok(count);
            }

            if let Some(block) = load_block(&transaction, self.inode, self.open_block_number)? {
                let end_point = amount_to_read + self.open_block_offset;
                let block_slice = &block[self.open_block_offset as usize..end_point as usize];
                (&mut buf[..amount_to_read as usize]).copy_from_slice(block_slice);
                self.open_block_offset += amount_to_read;
                if self.open_block_offset >= self.block_size {
                    self.open_block_number += 1;
                    self.open_block_offset -= self.block_size;
                }
            } else {
                (&mut buf[..amount_to_read as usize]).fill(0);
            }
            buf = &mut buf[amount_to_read as usize..];
            count += amount_to_read as usize;
        }
        transaction.commit().map_err(Error::from)?;
        Ok(count)
    }
}

impl Write for Handle {
    fn write(&mut self, mut buf: &[u8]) -> Result<usize, io::Error> {
        let connection = self.connection.upgrade().ok_or(Error::FilesystemClosed)?;
        let mut connection = connection.borrow_mut();

        let transaction = connection
            .transaction_with_behavior(TransactionBehavior::Immediate)
            .map_err(Error::from)?;
        let size = buf.len();
        while !buf.is_empty() {
            let mut block = load_block(&transaction, self.inode, self.open_block_number)?
                .unwrap_or_else(|| vec![0u8; self.block_size as usize]);
            let left_in_block = self.block_size - self.open_block_offset;
            let amount_to_write = buf.len().min(left_in_block as usize) as u32;
            let end_point = amount_to_write + self.open_block_offset;
            {
                let block_slice = &mut block[self.open_block_offset as usize..end_point as usize];
                block_slice.copy_from_slice(&buf[..amount_to_write as usize]);
            }
            store_block(&transaction, self.inode, self.open_block_number, &block)?;
            self.open_block_offset += amount_to_write;
            if self.open_block_offset >= self.block_size {
                self.open_block_number += 1;
                self.open_block_offset -= self.block_size;
            }
            buf = &buf[amount_to_write as usize..];
        }
        let new_size =
            self.open_block_number as u64 * self.block_size as u64 + self.open_block_offset as u64;
        set_file_size(&transaction, self.inode, new_size)?;
        transaction.commit().map_err(Error::from)?;
        Ok(size)
    }

    fn flush(&mut self) -> io::Result<()> {
        Ok(())
    }
}

impl io::Seek for Handle {
    fn seek(&mut self, pos: io::SeekFrom) -> Result<u64, io::Error> {
        fn seek_from_point(reference: u64, offset: i64) -> Result<u64, Error> {
            if offset >= 0 {
                reference
                    .checked_add(offset.unsigned_abs())
                    .ok_or(Error::PositionTooBig)
            } else {
                reference
                    .checked_sub(offset.unsigned_abs())
                    .ok_or(Error::PositionNegative)
            }
        }

        let connection = self.connection.upgrade().ok_or(Error::FilesystemClosed)?;
        let mut connection = connection.borrow_mut();

        // Read transaction just for efficient read behavior
        let mut transaction = connection.transaction().map_err(Error::from)?;
        transaction.set_drop_behavior(DropBehavior::Commit);

        let file_size = file_size(&transaction, self.inode)?;
        let new_position = match pos {
            io::SeekFrom::Start(offset) => offset,
            io::SeekFrom::End(offset) => seek_from_point(file_size, offset)?,
            io::SeekFrom::Current(offset) => seek_from_point(self.cursor_position(), offset)?,
        };
        self.set_cursor(new_position);

        Ok(new_position)
    }
}

impl Drop for Handle {
    fn drop(&mut self) {
        if let Some(connection) = self.connection.upgrade() {
            let mut connection = connection.borrow_mut();
            let transaction = connection
                .transaction_with_behavior(TransactionBehavior::Immediate)
                .unwrap();
            {
                let mut statement = transaction
                    .prepare_cached("DELETE FROM temp.open_inodes WHERE id = ?")
                    .unwrap();
                if let Err(e) = statement.execute([self.open_inode_id]) {
                    error!("closing file: {}", e);
                }
            }
            if let Err(e) = transaction.commit() {
                error!("closing file: {}", e);
            }
        }
    }
}
