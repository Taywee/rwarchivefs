use rusqlite::{
    types::{FromSql, FromSqlResult, ToSqlOutput, Value, ValueRef},
    ToSql,
};

#[derive(Copy, Clone, Hash, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct Mode(u32);

impl Mode {
    /* Make a mode from raw bits, filtering out illegal bits
     */
    pub fn from_raw(mode: u32) -> Self {
        Self(mode & 0o177777)
    }

    pub const fn new(kind: fuser::FileType, permissions: u16) -> Self {
        let kind_bits: u32 = match kind {
            fuser::FileType::NamedPipe => 0o0010000,
            fuser::FileType::CharDevice => 0o0020000,
            fuser::FileType::Directory => 0o0040000,
            fuser::FileType::BlockDevice => 0o0060000,
            fuser::FileType::RegularFile => 0o0100000,
            fuser::FileType::Symlink => 0o0120000,
            fuser::FileType::Socket => 0o0140000,
        };
        Self(kind_bits | (permissions as u32 & 0o7777))
    }

    pub fn kind(self) -> fuser::FileType {
        match self.0 & 0o170000 {
            0o0140000 => fuser::FileType::Socket,
            0o0120000 => fuser::FileType::Symlink,
            0o0100000 => fuser::FileType::RegularFile,
            0o0060000 => fuser::FileType::BlockDevice,
            0o0040000 => fuser::FileType::Directory,
            0o0020000 => fuser::FileType::CharDevice,
            0o0010000 => fuser::FileType::NamedPipe,
            _ => panic!("unrecognized kind"),
        }
    }

    pub fn permissions(self) -> u16 {
        self.0 as u16 & 0o7777u16
    }

    pub fn raw(self) -> u32 {
        self.0
    }
}

impl FromSql for Mode {
    fn column_result(value: ValueRef<'_>) -> FromSqlResult<Self> {
        u32::column_result(value).map(Mode::from_raw)
    }
}

impl ToSql for Mode {
    fn to_sql(&self) -> rusqlite::Result<ToSqlOutput<'_>> {
        Ok(ToSqlOutput::Owned(Value::Integer(self.raw().into())))
    }
}
