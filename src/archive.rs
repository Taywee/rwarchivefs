mod attributes;
mod directory_handle;
mod error;
mod file;
mod mode;

use crate::include_sql;

pub use self::attributes::{FileAttributes, InodeAttributes, SetInodeAttributes};

pub use directory_handle::DirectoryHandle;
pub use error::Error;
pub use file::Handle as FileHandle;
use fuser::FileType;
pub use mode::Mode;

use chrono::offset::Utc;
use rusqlite::{params, Connection, DropBehavior, OptionalExtension, TransactionBehavior};
use std::cell::RefCell;

use std::ffi::{OsStr, OsString};
use std::num::NonZeroU32;
use std::os::unix::ffi::{OsStrExt, OsStringExt};
use std::path::{Path, PathBuf};
use std::rc::Rc;

/// Set the given attribute to the given value if the optional is set.
macro_rules! set_attribute {
    ($connection:expr, $inode:expr, $name:literal, $value:expr) => {{
        let result: Result<(), Error> = $value.map_or(Ok(()), |value| {
            let mut statement = $connection.prepare_cached(concat!(
                "UPDATE inodes SET ",
                $name,
                "=? WHERE id=?"
            ))?;

            let changes = statement.execute(params![value, $inode])?;
            if changes > 0 {
                Ok(())
            } else {
                Err($crate::archive::error::Error::NotFound)
            }
        });
        result
    }};
}

macro_rules! set_time_attribute {
    ($connection:expr, $inode:expr, $name:literal, $value:expr) => {{
        let result: Result<(), Error> = $value.map_or(Ok(()), |value| {
            use std::time::UNIX_EPOCH;

            let mut statement = $connection.prepare_cached(concat!(
                "UPDATE inodes SET ",
                $name,
                "_seconds=?, ",
                $name,
                "_nanoseconds=? WHERE id=?"
            ))?;

            let (seconds, nanoseconds) = if value >= UNIX_EPOCH {
                let diff = value.duration_since(std::time::UNIX_EPOCH).unwrap();
                (diff.as_secs() as i64, diff.subsec_nanos())
            } else {
                let diff = std::time::UNIX_EPOCH.duration_since(value).unwrap();

                let nanoseconds = diff.subsec_nanos();
                if nanoseconds == 0 {
                    (-(diff.as_secs() as i64), 0)
                } else {
                    (-(diff.as_secs() as i64) - 1, 1_000_000_000 - nanoseconds)
                }
            };

            let changes = statement.execute(params![seconds, nanoseconds, $inode])?;
            if changes > 0 {
                Ok(())
            } else {
                Err(Error::NotFound)
            }
        });
        result
    }};
}

pub struct Archive {
    connection: Rc<RefCell<Connection>>,
}

#[derive(Debug)]
pub struct ArchiveConfig {
    pub block_size: NonZeroU32,
}

impl Default for ArchiveConfig {
    fn default() -> Self {
        ArchiveConfig {
            block_size: NonZeroU32::new(1024).unwrap(),
        }
    }
}

impl Archive {
    /// Open the archive and create initial tables if they don't exist.
    ///
    /// block_size only takes effect if it's a new archive.
    pub fn open<P: AsRef<Path>>(path: P, config: ArchiveConfig) -> Result<Archive, Error> {
        let path = path.as_ref();

        let mut connection = Connection::open(path)?;

        connection
            .query_row("PRAGMA journal_mode = WAL", [], |_| Ok(()))
            .optional()?;
        connection
            .query_row("PRAGMA synchronous = NORMAL", [], |_| Ok(()))
            .optional()?;
        connection
            .query_row("PRAGMA foreign_keys = ON", [], |_| Ok(()))
            .optional()?;
        connection
            .query_row("PRAGMA recursive_triggers = ON", [], |_| Ok(()))
            .optional()?;
        let application_id: i32 =
            connection.query_row("PRAGMA application_id", [], |row| row.get(0))?;

        match application_id {
            0 => {
                connection
                    .query_row(
                        &format!("PRAGMA application_id = {}", crate::APPLICATION_ID),
                        [],
                        |_| Ok(()),
                    )
                    .optional()?;
            }
            crate::APPLICATION_ID => (),
            _ => return Err(Error::WrongApplicationId),
        }

        let transaction = connection.transaction_with_behavior(TransactionBehavior::Exclusive)?;

        transaction.execute(include_sql!("create_table_metadata"), [])?;

        transaction.execute(
            "INSERT INTO metadata (id, block_size) VALUES (1, ?) ON CONFLICT DO NOTHING",
            [config.block_size.get()],
        )?;

        let start_database_version: u64 =
            transaction.query_row("SELECT database_version FROM metadata", [], |row| {
                row.get(0)
            })?;

        let mut database_version = start_database_version;

        if database_version < 1 {
            transaction.execute_batch(include_sql!("initialize_version_1"))?;

            let now = Utc::now();
            transaction.execute(
                include_sql!("insert_root_directory"),
                params![
                    Mode::new(fuser::FileType::Directory, 0o750),
                    now.timestamp(),
                    now.timestamp_subsec_nanos(),
                ],
            )?;

            database_version = 1;
        }

        transaction.execute("UPDATE metadata SET database_version=?", [database_version])?;

        transaction.execute_batch(include_sql!("cleanup_unlinked"))?;

        transaction.execute_batch(include_sql!("initialize_temp"))?;

        transaction.commit()?;

        Ok(Archive {
            connection: Rc::new(RefCell::new(connection)),
        })
    }

    pub fn open_dir(&self, inode: u64) -> DirectoryHandle {
        DirectoryHandle::new(inode, self.connection.clone())
    }

    pub fn attributes(&self, inode: u64) -> Result<FileAttributes, Error> {
        let mut connection = self.connection.borrow_mut();
        let mut transaction = connection.transaction()?;
        transaction.set_drop_behavior(DropBehavior::Commit);
        FileAttributes::for_inode(&transaction, inode)
    }

    pub fn lookup(&self, directory: u64, name: &OsStr) -> Result<FileAttributes, Error> {
        let mut connection = self.connection.borrow_mut();
        let mut transaction = connection.transaction()?;
        transaction.set_drop_behavior(DropBehavior::Commit);
        FileAttributes::for_directory_entry(&transaction, directory, name)
    }

    /// Create a file in a directory and get its attributes.
    fn inner_mknod(
        directory: u64,
        name: &OsStr,
        mode: Mode,
        connection: &Connection,
    ) -> Result<FileAttributes, Error> {
        let inode = {
            let mut statement = connection.prepare_cached(
                "SELECT inode FROM directory_entries WHERE directory = ? AND name = ?",
            )?;
            // Fail if it already exists
            statement
                .query_row(params![directory, name.as_bytes()], |row| row.get(0))
                .optional()?
                .map(|inode| Err(Error::AlreadyExists(inode)))
                .unwrap_or(Ok(()))?;
            let mut statement = connection.prepare_cached(include_sql!("insert_inode"))?;
            let now = Utc::now();
            statement.execute(params![mode, now.timestamp(), now.timestamp_subsec_nanos()])?;
            let inode = connection.last_insert_rowid() as u64;
            let mut statement = connection.prepare_cached(
                "INSERT INTO directory_entries (directory, name, inode) VALUES (?, ?, ?)",
            )?;
            statement.execute(params![directory, name.as_bytes(), inode])?;
            inode
        };
        let attributes = FileAttributes::for_inode(&connection, inode)?;
        Ok(attributes)
    }

    /// Create a file in a directory and get its attributes.
    pub fn mknod(&self, directory: u64, name: &OsStr, mode: Mode) -> Result<FileAttributes, Error> {
        let mut connection = self.connection.borrow_mut();
        let transaction = connection.transaction_with_behavior(TransactionBehavior::Immediate)?;
        let output = Self::inner_mknod(directory, name, mode, &transaction)?;
        transaction.commit()?;
        Ok(output)
    }

    /// Create a directory in a directory and get its attributes.
    pub fn mkdir(
        &self,
        directory: u64,
        name: &OsStr,
        permissions: u16,
    ) -> Result<FileAttributes, Error> {
        self.mknod(directory, name, Mode::new(FileType::Directory, permissions))
    }

    /// Create a file in a directory and get its attributes.
    pub fn unlink(&self, directory: u64, name: &OsStr) -> Result<(), Error> {
        let mut connection = self.connection.borrow_mut();
        let transaction = connection.transaction_with_behavior(TransactionBehavior::Immediate)?;
        {
            let mut statement = transaction
                .prepare_cached("DELETE FROM directory_entries WHERE directory = ? AND name = ?")?;
            statement.execute(params![directory, name.as_bytes()])?;
        }
        transaction.commit()?;
        Ok(())
    }

    pub fn open_file(&self, inode: u64) -> Result<FileHandle, Error> {
        FileHandle::new(inode, self.connection.clone())
    }

    pub fn set_attributes(
        &self,
        inode: u64,
        attributes: SetInodeAttributes,
    ) -> Result<FileAttributes, Error> {
        let mut connection = self.connection.borrow_mut();
        let transaction = connection.transaction()?;
        set_attribute!(transaction, inode, "mode", attributes.mode)?;
        set_attribute!(transaction, inode, "user_id", attributes.user_id)?;
        set_attribute!(transaction, inode, "group_id", attributes.group_id)?;
        set_time_attribute!(transaction, inode, "ctime", attributes.ctime)?;
        set_time_attribute!(transaction, inode, "atime", attributes.atime)?;
        set_time_attribute!(transaction, inode, "mtime", attributes.mtime)?;
        set_time_attribute!(transaction, inode, "btime", attributes.btime)?;

        if let Some(size) = attributes.size {
            file::set_file_size(&transaction, inode, size)?;
        }

        let attributes = FileAttributes::for_inode(&transaction, inode)?;
        transaction.commit()?;
        Ok(attributes)
    }

    /// Create a symlink in a directory and get its attributes.
    pub fn symlink(
        &self,
        directory: u64,
        name: &OsStr,
        link: &Path,
    ) -> Result<FileAttributes, Error> {
        let mut connection = self.connection.borrow_mut();
        let transaction = connection.transaction_with_behavior(TransactionBehavior::Immediate)?;
        let output = Self::inner_mknod(
            directory,
            name,
            Mode::new(FileType::Symlink, 0o777),
            &transaction,
        )?;
        {
            let mut create_symlink =
                transaction.prepare_cached("INSERT INTO symlinks (id, data) VALUES (?, ?)")?;
            create_symlink.execute(params![
                output.inode_attributes.id,
                link.as_os_str().as_bytes()
            ])?;
        }
        transaction.commit()?;
        Ok(output)
    }

    /// Create a symlink in a directory and get its attributes.
    pub fn readlink(&self, inode: u64) -> Result<PathBuf, Error> {
        let mut connection = self.connection.borrow_mut();
        let mut transaction = connection.transaction()?;
        transaction.set_drop_behavior(DropBehavior::Commit);
        let mut statement = transaction.prepare_cached("SELECT data FROM symlinks WHERE id=?")?;
        let data: Vec<u8> = statement.query_row(params![inode], |row| row.get(0))?;

        Ok(OsString::from_vec(data).into())
    }
}
