CREATE TABLE inodes (
    id INTEGER PRIMARY KEY NOT NULL,
    mode INTEGER NOT NULL,
    user_id INTEGER NOT NULL DEFAULT 0,
    group_id INTEGER NOT NULL DEFAULT 0,

    -- Size in bytes.
    -- Can not be computed from block size, because the final block may be incomplete,
    -- or sparse files might somehow have empty blocks at the end.
    size INTEGER NOT NULL DEFAULT 0,
    btime_seconds INTEGER NOT NULL,
    btime_nanoseconds INTEGER NOT NULL,
    ctime_seconds INTEGER NOT NULL,
    ctime_nanoseconds INTEGER NOT NULL,
    mtime_seconds INTEGER NOT NULL,
    mtime_nanoseconds INTEGER NOT NULL,
    atime_seconds INTEGER NOT NULL,
    atime_nanoseconds INTEGER NOT NULL
);

CREATE TABLE symlinks (
    id INTEGER PRIMARY KEY NOT NULL REFERENCES inodes(id) ON DELETE CASCADE,
    data BLOB NOT NULL
);

CREATE TABLE inode_compression (
    id INTEGER PRIMARY KEY NOT NULL REFERENCES inodes(id) ON DELETE CASCADE,
    data BLOB NOT NULL
);

CREATE TABLE file_blocks (
    id INTEGER PRIMARY KEY,
    inode INTEGER NOT NULL REFERENCES inodes(id) ON DELETE CASCADE,
    block_number INTEGER NOT NULL,
    data BLOB NOT NULL
);

CREATE UNIQUE INDEX file_blocks_index ON file_blocks(inode, block_number);

CREATE TABLE file_block_compression (
    id INTEGER PRIMARY KEY REFERENCES file_blocks (id) ON DELETE CASCADE,
    data BLOB NOT NULL
);

CREATE TABLE directory_entries (
    directory INTEGER NOT NULL REFERENCES inodes(id) ON DELETE RESTRICT,

    -- name must be a blob, because it is not necessarily valid UTF-8.  Thanks, Unix filesystem.
    name BLOB NOT NULL,
    inode INTEGER NOT NULL REFERENCES inodes(id) ON DELETE RESTRICT,
    PRIMARY KEY (directory, name ASC)
);

CREATE INDEX directory_entries_inodes ON directory_entries(inode);

CREATE TABLE extended_attributes (
    inode INTEGER NOT NULL REFERENCES inode(id) ON DELETE CASCADE,
    name TEXT NOT NULL,
    value BLOB NOT NULL,
    PRIMARY KEY (inode, name ASC)
);

-- Delete unneeded file blocks for truncated files.  The last block will still
-- need to have its trailing bytes turned to zeros in code for sanity's sake.
-- This could possibly be done with pure SQL for uncompressed files, but
-- compression will still need special handling regardless.
CREATE TRIGGER resize_file AFTER UPDATE ON inodes
    WHEN NEW.size < OLD.size
BEGIN
    DELETE FROM file_blocks WHERE
        inode=NEW.id
        -- Ceiling division
        AND block_number >= NEW.size / (SELECT block_size FROM metadata)
            + CASE NEW.size % (SELECT block_size FROM metadata) WHEN 0 THEN 0 ELSE 1 END;
END;
