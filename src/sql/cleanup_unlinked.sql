-- Clean up old dead inodes from an unclean unmount.
DELETE FROM inodes WHERE id != 1
    AND NOT EXISTS (SELECT 1 FROM directory_entries WHERE inode = inodes.id);
