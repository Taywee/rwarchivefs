SELECT
    mode,
    -- Count the links from directory entries, and add 1 for the root directory
    -- by default, as that should never drop to 0.
    (SELECT COUNT(*) FROM directory_entries WHERE inode = inodes.id)
        + CASE id WHEN 1 THEN 1 ELSE 0 END links,
    user_id,
    group_id,
    size,
    btime_seconds,
    btime_nanoseconds,
    ctime_seconds,
    ctime_nanoseconds,
    mtime_seconds,
    mtime_nanoseconds,
    atime_seconds,
    atime_nanoseconds
FROM inodes
    WHERE id = ?1
