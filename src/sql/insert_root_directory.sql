INSERT INTO inodes (
    id,
    mode,
    btime_seconds,
    btime_nanoseconds,
    ctime_seconds,
    ctime_nanoseconds,
    mtime_seconds,
    mtime_nanoseconds,
    atime_seconds,
    atime_nanoseconds
) VALUES (1, ?1, ?2, ?3, ?2, ?3, ?2, ?3, ?2, ?3)
