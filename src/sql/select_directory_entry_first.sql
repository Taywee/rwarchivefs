SELECT name, inode, mode
FROM directory_entries
JOIN inodes ON inodes.id = inode
WHERE directory = ?
ORDER BY NAME ASC
LIMIT 1
