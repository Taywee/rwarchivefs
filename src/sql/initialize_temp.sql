-- Open inode table kept in memory so we can use a trigger to delete inodes.
CREATE TABLE temp.open_inodes (
    id INTEGER PRIMARY KEY NOT NULL,

    -- Foreign keys may not reference across databases.
    inode INTEGER NOT NULL
);

CREATE INDEX temp.open_inodes_inodes ON open_inodes(inode);

-- Delete inodes when they are closed and have no links left
CREATE TEMP TRIGGER delete_inode_after_close AFTER DELETE ON temp.open_inodes
    WHEN
        old.inode != 1
        AND NOT EXISTS (SELECT 1 FROM temp.open_inodes WHERE inode = old.inode)
        AND NOT EXISTS (SELECT 1 FROM main.directory_entries WHERE inode = old.inode)
BEGIN
    DELETE FROM inodes WHERE id = old.inode;
END;

-- Delete inodes when they are unlinked and aren't open
CREATE TEMP TRIGGER delete_inode_after_unlink AFTER DELETE ON main.directory_entries
    WHEN
        old.inode != 1
        AND NOT EXISTS (SELECT 1 FROM temp.open_inodes WHERE inode = old.inode)
        AND NOT EXISTS (SELECT 1 FROM main.directory_entries WHERE inode = old.inode)
BEGIN
    DELETE FROM inodes WHERE id = old.inode;
END;
