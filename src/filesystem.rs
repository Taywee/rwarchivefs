use fuser::TimeOrNow;
use libc::ENOSYS;
use log::{debug, error, info};
use std::ffi::OsStr;
use std::io::{Read, Write};
use std::iter::Peekable;

use std::os::unix::prelude::OsStrExt;
use std::path::Path;
use std::time::Duration;
use std::time::SystemTime;

use crate::archive::{Archive, Mode, SetInodeAttributes};
use crate::archive::{DirectoryHandle, FileHandle};

pub struct Filesystem {
    archive: Archive,
}

impl Filesystem {
    pub fn new(archive: Archive) -> Filesystem {
        Filesystem { archive }
    }
}

impl fuser::Filesystem for Filesystem {
    fn init(
        &mut self,
        _req: &fuser::Request<'_>,
        _config: &mut fuser::KernelConfig,
    ) -> Result<(), libc::c_int> {
        Ok(())
    }

    fn lookup(
        &mut self,
        _req: &fuser::Request<'_>,
        parent: u64,
        name: &OsStr,
        reply: fuser::ReplyEntry,
    ) {
        let attributes = self.archive.lookup(parent, name);
        match attributes {
            Ok(attributes) => reply.entry(&Duration::ZERO, &attributes.into(), 0),
            Err(e) => {
                error!("lookup: {}", e);
                reply.error(e.errno());
            }
        }
    }

    fn forget(&mut self, _req: &fuser::Request<'_>, _ino: u64, _nlookup: u64) {}

    fn getattr(&mut self, _req: &fuser::Request<'_>, ino: u64, reply: fuser::ReplyAttr) {
        let attributes = match self.archive.attributes(ino) {
            Ok(a) => a,
            Err(e) => {
                error!("getattr: {}", e);
                reply.error(e.errno());
                return;
            }
        };
        reply.attr(&Duration::ZERO, &attributes.into());
    }

    fn setattr(
        &mut self,
        _req: &fuser::Request<'_>,
        inode: u64,
        mode: Option<u32>,
        user_id: Option<u32>,
        group_id: Option<u32>,
        size: Option<u64>,
        atime: Option<TimeOrNow>,
        mtime: Option<TimeOrNow>,
        ctime: Option<SystemTime>,
        _fh: Option<u64>,
        crtime: Option<SystemTime>,
        _chgtime: Option<SystemTime>,
        _bkuptime: Option<SystemTime>,
        _flags: Option<u32>,
        reply: fuser::ReplyAttr,
    ) {
        let now = SystemTime::now();
        let system_time = move |time: TimeOrNow| -> SystemTime {
            match time {
                TimeOrNow::SpecificTime(time) => time,
                TimeOrNow::Now => now,
            }
        };

        let set_attributes = SetInodeAttributes {
            mode: mode.map(Mode::from_raw),
            user_id,
            group_id,
            btime: crtime,
            ctime,
            mtime: mtime.map(system_time),
            atime: atime.map(system_time),
            size,
        };

        match self.archive.set_attributes(inode, set_attributes) {
            Ok(attributes) => reply.attr(&Duration::ZERO, &attributes.into()),
            Err(e) => {
                error!("setattr: {}", e);
                reply.error(e.errno());
            }
        }
    }

    fn readlink(&mut self, _req: &fuser::Request<'_>, inode: u64, reply: fuser::ReplyData) {
        match self.archive.readlink(inode) {
            Ok(data) => reply.data(data.as_os_str().as_bytes()),
            Err(e) => {
                error!("readlink: {}", e);
                reply.error(e.errno());
            }
        }
    }

    fn mknod(
        &mut self,
        _req: &fuser::Request<'_>,
        parent: u64,
        name: &OsStr,
        mode: u32,
        _umask: u32,
        _rdev: u32,
        reply: fuser::ReplyEntry,
    ) {
        info!("mknod: {}, {}, {}", parent, name.to_string_lossy(), mode);
        match self.archive.mknod(parent, name, Mode::from_raw(mode)) {
            Ok(attributes) => reply.entry(&Duration::ZERO, &attributes.into(), 0),
            Err(e) => {
                error!("mknod: {}", e);
                reply.error(e.errno());
            }
        }
    }

    fn mkdir(
        &mut self,
        _req: &fuser::Request<'_>,
        parent: u64,
        name: &OsStr,
        mode: u32,
        _umask: u32,
        reply: fuser::ReplyEntry,
    ) {
        match self.archive.mkdir(parent, name, mode as u16) {
            Ok(attributes) => reply.entry(&Duration::ZERO, &attributes.into(), 0),
            Err(e) => {
                error!("mknod: {}", e);
                reply.error(e.errno());
            }
        }
    }

    fn unlink(
        &mut self,
        _req: &fuser::Request<'_>,
        parent: u64,
        name: &OsStr,
        reply: fuser::ReplyEmpty,
    ) {
        match self.archive.unlink(parent, name) {
            Ok(()) => reply.ok(),
            Err(e) => {
                error!("unlink: {}", e);
                reply.error(e.errno());
            }
        }
    }

    fn rmdir(
        &mut self,
        _req: &fuser::Request<'_>,
        parent: u64,
        name: &OsStr,
        reply: fuser::ReplyEmpty,
    ) {
        match self.archive.unlink(parent, name) {
            Ok(()) => reply.ok(),
            Err(e) => {
                error!("rmdir: {}", e);
                reply.error(e.errno());
            }
        }
    }

    fn symlink(
        &mut self,
        _req: &fuser::Request<'_>,
        parent: u64,
        name: &OsStr,
        link: &Path,
        reply: fuser::ReplyEntry,
    ) {
        match self.archive.symlink(parent, name, link) {
            Ok(attributes) => reply.entry(&Duration::ZERO, &attributes.into(), 0),
            Err(e) => {
                error!("symlink: {}", e);
                reply.error(e.errno());
            }
        }
    }

    fn rename(
        &mut self,
        _req: &fuser::Request<'_>,
        parent: u64,
        name: &OsStr,
        newparent: u64,
        newname: &OsStr,
        flags: u32,
        reply: fuser::ReplyEmpty,
    ) {
        debug!(
            "[Not Implemented] rename(parent: {:#x?}, name: {:?}, newparent: {:#x?}, \
            newname: {:?}, flags: {})",
            parent, name, newparent, newname, flags,
        );
        reply.error(ENOSYS);
    }

    fn link(
        &mut self,
        _req: &fuser::Request<'_>,
        ino: u64,
        newparent: u64,
        newname: &OsStr,
        reply: fuser::ReplyEntry,
    ) {
        debug!(
            "[Not Implemented] link(ino: {:#x?}, newparent: {:#x?}, newname: {:?})",
            ino, newparent, newname
        );
        reply.error(libc::EPERM);
    }

    fn open(
        &mut self,
        _req: &fuser::Request<'_>,
        inode: u64,
        _flags: i32,
        reply: fuser::ReplyOpen,
    ) {
        match self.archive.open_file(inode) {
            Ok(handle) => {
                let handle = Box::new(handle);
                reply.opened(Box::into_raw(handle) as usize as u64, 0);
            }
            Err(e) => {
                error!("open: {}", e);
                reply.error(e.errno());
            }
        }
    }

    fn read(
        &mut self,
        _req: &fuser::Request<'_>,
        _ino: u64,
        fh: u64,
        _offset: i64,
        size: u32,
        _flags: i32,
        _lock_owner: Option<u64>,
        reply: fuser::ReplyData,
    ) {
        let fh = unsafe { &mut *(fh as usize as *mut FileHandle) };
        let mut data = vec![0u8; size as usize];
        match fh.read(&mut data) {
            Ok(_) => reply.data(&data),
            Err(e) => {
                error!("read: {}", e);
                reply.error(e.raw_os_error().unwrap_or(libc::EIO));
            }
        }
    }

    fn write(
        &mut self,
        _req: &fuser::Request<'_>,
        _inode: u64,
        fh: u64,
        _offset: i64,
        data: &[u8],
        _write_flags: u32,
        _flags: i32,
        _lock_owner: Option<u64>,
        reply: fuser::ReplyWrite,
    ) {
        let fh = unsafe { &mut *(fh as usize as *mut FileHandle) };
        match fh.write(data) {
            Ok(amount) => reply.written(amount as u32),
            Err(e) => {
                error!("write: {}", e);
                reply.error(e.raw_os_error().unwrap_or(libc::EIO));
            }
        }
    }

    fn flush(
        &mut self,
        _req: &fuser::Request<'_>,
        ino: u64,
        fh: u64,
        lock_owner: u64,
        reply: fuser::ReplyEmpty,
    ) {
        debug!(
            "[Not Implemented] flush(ino: {:#x?}, fh: {}, lock_owner: {:?})",
            ino, fh, lock_owner
        );
        reply.error(ENOSYS);
    }

    fn release(
        &mut self,
        _req: &fuser::Request<'_>,
        _ino: u64,
        fh: u64,
        _flags: i32,
        _lock_owner: Option<u64>,
        _flush: bool,
        reply: fuser::ReplyEmpty,
    ) {
        unsafe {
            let _ = Box::from_raw(fh as usize as *mut FileHandle);
        }
        reply.ok();
    }

    fn fsync(
        &mut self,
        _req: &fuser::Request<'_>,
        ino: u64,
        fh: u64,
        datasync: bool,
        reply: fuser::ReplyEmpty,
    ) {
        debug!(
            "[Not Implemented] fsync(ino: {:#x?}, fh: {}, datasync: {})",
            ino, fh, datasync
        );
        reply.error(ENOSYS);
    }

    fn opendir(
        &mut self,
        _req: &fuser::Request<'_>,
        inode: u64,
        _flags: i32,
        reply: fuser::ReplyOpen,
    ) {
        let handle = Box::new(self.archive.open_dir(inode).peekable());
        reply.opened(Box::into_raw(handle) as usize as u64, 0);
    }

    fn readdir(
        &mut self,
        _req: &fuser::Request<'_>,
        ino: u64,
        fh: u64,
        _offset: i64,
        mut reply: fuser::ReplyDirectory,
    ) {
        let handle = unsafe { &mut *(fh as usize as *mut Peekable<DirectoryHandle>) };

        while let Some(entry) = handle.peek() {
            let entry = match entry {
                Ok(entry) => entry,
                Err(e) => {
                    error!("readdir error: {}", e);
                    reply.error(e.errno());
                    return;
                }
            };
            if reply.add(ino, 0, entry.mode().kind(), entry.name()) {
                // Full, leave peaked value
                break;
            } else {
                handle.next();
            }
        }
        reply.ok();
    }

    fn readdirplus(
        &mut self,
        _req: &fuser::Request<'_>,
        ino: u64,
        fh: u64,
        offset: i64,
        reply: fuser::ReplyDirectoryPlus,
    ) {
        debug!(
            "[Not Implemented] readdirplus(ino: {:#x?}, fh: {}, offset: {})",
            ino, fh, offset
        );
        reply.error(ENOSYS);
    }

    fn releasedir(
        &mut self,
        _req: &fuser::Request<'_>,
        _ino: u64,
        fh: u64,
        _flags: i32,
        reply: fuser::ReplyEmpty,
    ) {
        unsafe {
            let _ = Box::from_raw(fh as usize as *mut Peekable<DirectoryHandle>);
        }
        reply.ok();
    }

    fn fsyncdir(
        &mut self,
        _req: &fuser::Request<'_>,
        ino: u64,
        fh: u64,
        datasync: bool,
        reply: fuser::ReplyEmpty,
    ) {
        debug!(
            "[Not Implemented] fsyncdir(ino: {:#x?}, fh: {}, datasync: {})",
            ino, fh, datasync
        );
        reply.error(ENOSYS);
    }

    fn statfs(&mut self, _req: &fuser::Request<'_>, _ino: u64, reply: fuser::ReplyStatfs) {
        reply.statfs(0, 0, 0, 0, 0, 512, 255, 0);
    }

    fn setxattr(
        &mut self,
        _req: &fuser::Request<'_>,
        ino: u64,
        name: &OsStr,
        _value: &[u8],
        flags: i32,
        position: u32,
        reply: fuser::ReplyEmpty,
    ) {
        debug!(
            "[Not Implemented] setxattr(ino: {:#x?}, name: {:?}, flags: {:#x?}, position: {})",
            ino, name, flags, position
        );
        reply.error(ENOSYS);
    }

    fn getxattr(
        &mut self,
        _req: &fuser::Request<'_>,
        ino: u64,
        name: &OsStr,
        size: u32,
        reply: fuser::ReplyXattr,
    ) {
        debug!(
            "[Not Implemented] getxattr(ino: {:#x?}, name: {:?}, size: {})",
            ino, name, size
        );
        reply.error(ENOSYS);
    }

    fn listxattr(
        &mut self,
        _req: &fuser::Request<'_>,
        ino: u64,
        size: u32,
        reply: fuser::ReplyXattr,
    ) {
        debug!(
            "[Not Implemented] listxattr(ino: {:#x?}, size: {})",
            ino, size
        );
        reply.error(ENOSYS);
    }

    fn removexattr(
        &mut self,
        _req: &fuser::Request<'_>,
        ino: u64,
        name: &OsStr,
        reply: fuser::ReplyEmpty,
    ) {
        debug!(
            "[Not Implemented] removexattr(ino: {:#x?}, name: {:?})",
            ino, name
        );
        reply.error(ENOSYS);
    }

    fn access(&mut self, _req: &fuser::Request<'_>, ino: u64, mask: i32, reply: fuser::ReplyEmpty) {
        debug!("[Not Implemented] access(ino: {:#x?}, mask: {})", ino, mask);
        reply.error(ENOSYS);
    }

    fn create(
        &mut self,
        _req: &fuser::Request<'_>,
        _parent: u64,
        _name: &OsStr,
        _mode: u32,
        _umask: u32,
        _flags: i32,
        reply: fuser::ReplyCreate,
    ) {
        reply.error(ENOSYS);
    }

    fn getlk(
        &mut self,
        _req: &fuser::Request<'_>,
        ino: u64,
        fh: u64,
        lock_owner: u64,
        start: u64,
        end: u64,
        typ: i32,
        pid: u32,
        reply: fuser::ReplyLock,
    ) {
        debug!(
            "[Not Implemented] getlk(ino: {:#x?}, fh: {}, lock_owner: {}, start: {}, \
            end: {}, typ: {}, pid: {})",
            ino, fh, lock_owner, start, end, typ, pid
        );
        reply.error(ENOSYS);
    }

    fn setlk(
        &mut self,
        _req: &fuser::Request<'_>,
        ino: u64,
        fh: u64,
        lock_owner: u64,
        start: u64,
        end: u64,
        typ: i32,
        pid: u32,
        sleep: bool,
        reply: fuser::ReplyEmpty,
    ) {
        debug!(
            "[Not Implemented] setlk(ino: {:#x?}, fh: {}, lock_owner: {}, start: {}, \
            end: {}, typ: {}, pid: {}, sleep: {})",
            ino, fh, lock_owner, start, end, typ, pid, sleep
        );
        reply.error(ENOSYS);
    }

    fn bmap(
        &mut self,
        _req: &fuser::Request<'_>,
        ino: u64,
        blocksize: u32,
        idx: u64,
        reply: fuser::ReplyBmap,
    ) {
        debug!(
            "[Not Implemented] bmap(ino: {:#x?}, blocksize: {}, idx: {})",
            ino, blocksize, idx,
        );
        reply.error(ENOSYS);
    }

    fn ioctl(
        &mut self,
        _req: &fuser::Request<'_>,
        ino: u64,
        fh: u64,
        flags: u32,
        cmd: u32,
        in_data: &[u8],
        out_size: u32,
        reply: fuser::ReplyIoctl,
    ) {
        debug!(
            "[Not Implemented] ioctl(ino: {:#x?}, fh: {}, flags: {}, cmd: {}, \
            in_data.len(): {}, out_size: {})",
            ino,
            fh,
            flags,
            cmd,
            in_data.len(),
            out_size,
        );
        reply.error(ENOSYS);
    }

    fn fallocate(
        &mut self,
        _req: &fuser::Request<'_>,
        ino: u64,
        fh: u64,
        offset: i64,
        length: i64,
        mode: i32,
        reply: fuser::ReplyEmpty,
    ) {
        debug!(
            "[Not Implemented] fallocate(ino: {:#x?}, fh: {}, offset: {}, \
            length: {}, mode: {})",
            ino, fh, offset, length, mode
        );
        reply.error(ENOSYS);
    }

    fn lseek(
        &mut self,
        _req: &fuser::Request<'_>,
        ino: u64,
        fh: u64,
        offset: i64,
        whence: i32,
        reply: fuser::ReplyLseek,
    ) {
        debug!(
            "[Not Implemented] lseek(ino: {:#x?}, fh: {}, offset: {}, whence: {})",
            ino, fh, offset, whence
        );
        reply.error(ENOSYS);
    }

    fn copy_file_range(
        &mut self,
        _req: &fuser::Request<'_>,
        ino_in: u64,
        fh_in: u64,
        offset_in: i64,
        ino_out: u64,
        fh_out: u64,
        offset_out: i64,
        len: u64,
        flags: u32,
        reply: fuser::ReplyWrite,
    ) {
        debug!(
            "[Not Implemented] copy_file_range(ino_in: {:#x?}, fh_in: {}, \
            offset_in: {}, ino_out: {:#x?}, fh_out: {}, offset_out: {}, \
            len: {}, flags: {})",
            ino_in, fh_in, offset_in, ino_out, fh_out, offset_out, len, flags
        );
        reply.error(ENOSYS);
    }
}
