pub mod archive;
pub mod filesystem;

const APPLICATION_ID: i32 = 2118388174;

#[macro_export]
macro_rules! include_sql {
    ($name:expr) => {
        include_str!(concat!(
            env!("CARGO_MANIFEST_DIR"),
            "/src/sql/",
            $name,
            ".sql"
        ))
    };
}
