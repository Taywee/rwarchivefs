# rwarchivefs

A read-write mountable archive filesystem, similar to squashfs, but read-write
and more wasteful, based on fuse and a sqlite archive.

The primary goal of this is that it should be fun to make, and should be
educational for future work with rwtagfs.

This will be a filesystem as a sqlite archive, but intended to be compressable
and easy to work with both as a live filesystem and as an archive that can be
easily extracted through Sqlite.

This will have the following features:

* Mountable as a FUSE filesystem
* Extractable as an archive
* Full POSIX ACLs, attributes, and extended attributes
* Block-based file storage for more efficient file writing, and sparse files.
* Full compression support, with each block individually compressed, so a single
  file can be saved with different compressions, and changing compression
  doesn't break anything.
* Full consistency ensured by SQLite transactions.

Basically, we want an efficient, compact, read-writable squashfs as a SQLite
archive, with a reference implementation written in Rust.
